import _axios from 'axios'
export  class Http {
   private static axios = _axios.create({
       baseURL: 'https://reqres.in/api/',
   });
   static  async get(url:string){
       try {
          const response = await Http.axios.get(url);
          if(response){
              return  response.data;
          }
       }catch (e) {
          Http.handleErrors(e);
          return Promise.reject(e);
       }
   }
   static async post(url:string,body:object){
       try {
           const response = await Http.axios.post(url,body);
           if(response){
               return  response.data;
           }
       }catch (e) {
           Http.handleErrors(e);
           return  Promise.reject(e);
       }
   }
  private static handleErrors(error: any){
       if(error.response){
           const message = error.response.data.message;
           const errorMessage = message ? message : 'something went wrong.'
           console.log(errorMessage);
       }else {
           console.log('something went wrong');
       }
   }
}
