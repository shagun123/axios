var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import _axios from 'axios';
export class Http {
    static get(url) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield Http.axios.get(url);
                if (response) {
                    return response.data;
                }
            }
            catch (e) {
                Http.handleErrors(e);
                return Promise.reject(e);
            }
        });
    }
    static post(url, body) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield Http.axios.post(url, body);
                if (response) {
                    return response.data;
                }
            }
            catch (e) {
                Http.handleErrors(e);
                return Promise.reject(e);
            }
        });
    }
    static handleErrors(error) {
        if (error.response) {
            const message = error.response.data.message;
            const errorMessage = message ? message : 'something went wrong.';
            console.log(errorMessage);
        }
        else {
            console.log('something went wrong');
        }
    }
}
Http.axios = _axios.create({
    baseURL: 'https://reqres.in/api/',
});
